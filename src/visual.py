import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
from sklearn.ensemble import RandomForestRegressor
from sklearn.linear_model import LinearRegression, Ridge, Lasso
from sklearn.metrics import r2_score, mean_squared_error
from sklearn.svm import SVR

from load_data import fullloader, brandloader, pureloader
from compute import compute_show_errors


def load_df():
    df = pd.read_csv('data/2023_MCM_data_all.csv')
    return df

def heatMap():
    df = load_df()
    print(df.columns)
    df = df[['Length (ft)', 'Displacement (lbs)', 'LWL (ft)', 'Beam (ft)', 'Draft (ft)', 'Sail Area (sq ft)', 'is_mono']]
    labels = ['Length', 'Displacement', 'LWL', 'Beam', 'Draft', 'Sail Area', 'is Mono']
    corr = df.corr()
    figure, ax = plt.subplots(figsize=(corr.shape[0], corr.shape[0]))
    sns.heatmap(corr, square=True, annot=True, ax=ax, cmap="YlGnBu", xticklabels=[], yticklabels=labels)
    plt.show()

def predMap(y_pred, y_true, reallabel='real', predlabel='pred', show=True):
    x_plot = [i for i in range(len(y_pred))]
    plt.plot(x_plot, y_true, label=reallabel)
    plt.plot(x_plot, y_pred, label=predlabel)
    plt.legend()
    if show:
        plt.show()

def PPplot():
    # 用PP图绘制listing price和理论分布的一致性，和price取对数的PP图对比，发现取对数更加符合分布情况。（PP图意义不明）
    df = load_df()
    col = 'Listing Price (USD)'
    df['CDF'] = df[col].rank(pct=True)
    df[col] = df[col].apply(lambda x:np.log(x))

    # 绘制PP图
    sns.scatterplot(x='CDF', y=col, data=df)
    plt.show()

def BoxPlot_geo_list():
    # 建立区域对定价的影响关系模型
    Monohulled = pd.read_excel('data//Monohulled Sailboats_final.xlsx')
    Catamarans = pd.read_excel('data//Catamarans_final.xlsx')
    Monohulled['is_mono'] = 1
    Catamarans['is_mono'] = 0
    df = pd.concat([Monohulled, Catamarans], axis=0)
    fig = plt.figure(1, figsize=(7,7))
    df = df.dropna()
    col = 'Listing Price (USD)'
    df = df[df[col] < df[col].quantile(0.98)]
    # df[col] = df[col].apply(lambda x: np.log(x))
    sns.boxplot(df, x='Geographic Region', y='Listing Price (USD)')
    plt.show()

def load_from_final(dropna=False):
    df = pd.read_csv('data//final_dataset.csv')
    if dropna:
        df = df.dropna()
    X  = df.drop('Listing Price (USD)',axis=1).values
    Y  = df['Listing Price (USD)'].values
    return X, Y, df

def visual_price_disp():
    var = 'Listing Price (USD)'
    sns.set_palette('Oranges_r')
    df = pd.read_csv('data//2023_MCM_data_all.csv')
    plt.figure(figsize=(9,9))
    sns.displot(df, x=var, kde='True')
    plt.show()
    tmp= df[var].quantile(0.98)  # 消除长尾
    df = df[df[var] < tmp]
    sns.displot(df, x=var, kde='True')
    plt.show()

def visual_rf_importance():
    def withsort(a, b):
        sorted_pairs = sorted(zip(a, b))
        # 将排序后的结果分离成两个列表
        a_sorted, b_sorted = zip(*sorted_pairs)
        return a_sorted, b_sorted
    plt.figure(figsize=(8,6))
    X, Y, df = load_from_final()
    attr_cols = list(df.drop('Listing Price (USD)', axis=1).columns)
    reg = RandomForestRegressor(n_estimators=20)
    reg = reg.fit(X, Y)
    attr_importance = reg.feature_importances_
    print(attr_importance.shape)
    attr_importance, attr_cols = withsort(attr_importance, attr_cols)
    # df = pd.DataFrame({'x': np.log(100*attr_importance), 'y':attr_cols})
    df.to_csv('data//rf_importance.csv')
    # plt.barh(range(len(attr_importance)), attr_importance)
    #
    # # 设置y轴标签
    # plt.yticks(range(len(attr_importance)), attr_cols, fontsize=8)
    # plt.show()
    # plt.pie(attr_importance, labels=attr_cols)
    print(attr_cols)
    plt.pie(attr_importance, autopct='%1.1f%%', labels=attr_cols, explode=[1,1,1,1,0.5,0,0,0,0,0,0,0])
    plt.show()

def visual_rf_hyperparam():
    # 利用k_fold折交叉验证检验最好的超参数设置
    k_fold = 5
    n_candidates = [i for i in range(1, 40)]
    X, Y, _ = load_from_final(dropna=True)
    r2score = []
    indexs = [i for i in range(len(X))]
    np.random.shuffle(indexs)
    period = int(len(indexs) / k_fold) # 每段长度
    r2s = []
    for n in n_candidates:
        reg = RandomForestRegressor(n_estimators=n)
        # print('***estimator=%d***' % n)
        r2 = 0
        for i in range(k_fold):
            train_index, test_index =indexs[:period*i]+indexs[period*(i+1):], indexs[period*i:period*(i+1)]
            train_x, test_x = X[train_index], X[test_index]
            train_y, test_y = Y[train_index], Y[test_index]
            cur_reg = reg.fit(train_x, train_y)
            pred_y = cur_reg.predict(test_x)
            cur_r2 = r2_score(test_y, pred_y)
            # print('Fold-%d, r2score=%.3f'%(i, cur_r2))
            r2 += cur_r2
        print('r2score on %d_estimator=%.3f' % (n, r2/k_fold))
        r2s.append(r2/k_fold)
    plt.plot(n_candidates, r2s)
    plt.scatter(r2s.index(max(r2s)), max(r2s), marker='x', color='red')
    plt.xlabel('k-estimators')
    plt.ylabel('R2')
    plt.show()

def visual_k_fold_validation():
    # 利用k_fold折交叉验证检验模型
    k_fold = 10
    X, Y, _ = fullloader()
    r2score = []
    indexs = [i for i in range(len(X))]
    np.random.shuffle(indexs)
    period = int(len(indexs) / k_fold) # 每段长度
    r2s = []
    x_plot = []
    reg = RandomForestRegressor(n_estimators=40)
    # print('***estimator=%d***' % n)
    r2 = 0
    for i in range(k_fold):
        train_index, test_index =indexs[:period*i]+indexs[period*(i+1):], indexs[period*i:period*(i+1)]
        train_x, test_x = X[train_index], X[test_index]
        train_y, test_y = Y[train_index], Y[test_index]
        cur_reg = reg.fit(train_x, train_y)
        pred_y = cur_reg.predict(test_x)
        cur_r2 = r2_score(np.exp(test_y), np.exp(pred_y))
        print('Fold-%d, r2score=%.3f'%(i, cur_r2))
        r2s.append(cur_r2)
        x_plot.append((i))
    # df = pd.DataFrame({'fold-k':x_plot, 'R2-score':r2s})
    # sns.lineplot(df, x='fold-k', y='R2-score', )
    plt.plot(x_plot, r2s)
    plt.scatter(x_plot, r2s, marker='x')
    plt.ylim(0, 1)
    plt.xlabel('kth-fold')
    plt.ylabel('R2-score')
    plt.title('%d-Fold-Validation'%k_fold)
    plt.show()

def visual_HongkongPredict(model='combined'):
    def combinedModel():
        reg = RandomForestRegressor(n_estimators=100)
        X, Y, _ = fullloader('Both')
        reg.fit(X, Y)
        return reg
    df = pd.read_excel('data//Hongkong_Boats.xlsx')
    df = df[df['Year'] < 2023]
    df['Year'] = df['Year'].apply(lambda x: 2020-x)
    Y_test = df['Listing Price (USD)'].apply(lambda x: np.log(x)).values
    reg = combinedModel()
    X_df = df.drop(['Make', 'Variant', 'Listing Price (USD)'], axis=1)
    region = 'Geographic Region_USA' # 三者都差不多，用USA吧
    X_df[['Geographic Region_USA', 'Geographic Region_Europe', 'Geographic Region_Caribbean']] = 0
    X_df[region] = 1     # 地区属性的某一列为1
    print(X_df.columns)
    X_test = X_df.values
    Y_pred = reg.predict(X_test)
    realabel = 'Listing Price'
    predMap(y_pred=np.exp(Y_pred), y_true=np.exp(Y_test), reallabel=realabel, predlabel='Prediction', show=False)
    compute_show_errors(np.exp(Y_test), np.exp(Y_pred))
    plt.legend()
    plt.show()

def compareModels():
    monoX, monoY, _ = fullloader('Monohulled')
    cataX, cataY, _ = fullloader('Catamarans')
    train_per = 0.9
    monoindexs = [i for i in range(len(monoX))]
    cataindexs = [i for i in range(len(cataX))]
    np.random.shuffle(monoindexs)
    np.random.shuffle(cataindexs)
    mono_train_len = int(train_per * len(monoX))
    cata_train_len = int(train_per * len(cataX))
    mono_train_idx, mono_test_idx = monoindexs[:mono_train_len], monoindexs[mono_train_len:]
    cata_train_idx, cata_test_idx = cataindexs[:cata_train_len], cataindexs[cata_train_len:]
    monoXtrain, monoXtest = monoX[mono_train_idx], monoX[mono_test_idx]
    monoYtrain, monoYtest = monoY[mono_train_idx], monoY[mono_test_idx]
    cataXtrain, cataXtest = cataX[cata_train_idx], cataX[cata_test_idx]
    cataYtrain, cataYtest = cataY[cata_train_idx], cataY[cata_test_idx]

    Ytest = np.concatenate([monoYtest, cataYtest], axis=0)

    combinedModel = RandomForestRegressor(n_estimators=100)
    combinedModel = combinedModel.fit(np.concatenate([monoXtrain, cataXtrain], axis=0), np.concatenate([monoYtrain, cataYtrain], axis=0))
    combinedPredMono  = combinedModel.predict(monoXtest)
    combinedPredCata  = combinedModel.predict(cataXtest)
    combinedPred = np.concatenate([combinedPredMono, combinedPredCata], axis=0)
    separateModel = [RandomForestRegressor(n_estimators=100), RandomForestRegressor(n_estimators=100)]
    separateModel[0].fit(cataXtrain, cataYtrain) # is_mono=0,是cata
    separateModel[1].fit(monoXtrain, monoYtrain)
    separatePredMono = separateModel[1].predict(monoXtest)
    separatePredCata = separateModel[0].predict(cataXtest)
    separatePred = np.concatenate([separatePredMono, separatePredCata], axis=0)

    fig = plt.figure(figsize=(10,20))
    # fig.canvas.manager.full_screen_toggle()
    # 比较Mono预测情况
    plt.subplot(311)
    # plt.xlabel('Monohulled')
    predMap(combinedPredMono, monoYtest, 'Real Price', 'combined prediction', False)
    # plt.subplot(122)
    predMap(separatePredMono, monoYtest, '', 'separate prediction', False)
    print('[Mono] combined MSE=%.4f' % mean_squared_error(monoYtest, combinedPredMono, squared=False))
    print('[Mono] separate MSE=%.4f' % mean_squared_error(monoYtest, separatePredMono, squared=False))

    # 比较Cata预测情况
    plt.subplot(312)
    # plt.xlabel('Catamaran')
    predMap(combinedPredCata, cataYtest, 'Real Price', 'combined prediction', False)
    predMap(separatePredCata, cataYtest, '', 'separate prediction', False)
    print('[Cata] combined MSE=%.4f' % mean_squared_error(cataYtest, combinedPredCata, squared=False))
    print('[Cata] separate MSE=%.4f' % mean_squared_error(cataYtest, separatePredCata, squared=False))

    # 比较总体预测情况
    plt.subplot(313)
    # plt.xlabel('ALL')
    predMap(combinedPred, Ytest, 'Real Price', 'combined prediction', False)
    predMap(separatePred, Ytest, '', 'separate prediction', True)
    print('[ALL] combined MSE=%.4f' % mean_squared_error(Ytest, combinedPred, squared=False))
    print('[ALL] separate MSE=%.4f' % mean_squared_error(Ytest, separatePred, squared=False))
    print('[ALL] combined R2score=%.4f' % r2_score(Ytest, combinedPred))
    print('[ALL] separate R2score=%.4f' % r2_score(Ytest, separatePred))

    return combinedModel, separateModel

def visual_attribute_distribution():
    df = load_df()
    plt.style.use('ggplot')
    df = df.drop(['Listing Price (USD)', 'is_mono'], axis=1) # 删除因变量，字符串变量和分类变量
    cols = ['Length (ft)', 'LWL (ft)', 'Beam (ft)', 'Draft (ft)',
            'Displacement (lbs)', 'Sail Area (sq ft)', 'Average Cargo Throughput (tons)', 'GDP (USD billion)',
            'GDP per capita (USD)','Average ratio of total logistics costs to GDP']
    for i in range(len(cols)):
        plt.subplot(2, 5, i + 1)
        df[cols[i]].hist(bins=20)
    plt.show()

def visual_price_ismono():
    _, _, df = load_from_final(dropna=True)
    mono = df[df['is_mono']==1]
    cata = df[df['is_mono']==0]
    price = 'Listing Price (USD)'
    plt.plot(mono[price].values, mono['Year'].values)
    plt.plot(mono[price].values, mono['Year'].values)
    sns.displot(cata, x=price)
    plt.show()

def visual_MakeVariant_freq():
    Monohulled = pd.read_excel('data//Monohulled Sailboats_final.xlsx')
    Catamarans = pd.read_excel('data//Catamarans_final.xlsx')
    # df = pd.concat([Monohulled, Catamarans], axis=0)              # 选择想要展示的类别
    df = Monohulled
    # df = Catamarans
    df = df.dropna()
    counts = df['Make Variant'].value_counts()
    counts = counts[counts > 30]
    ax = sns.barplot(x=counts.index, y=counts.values)
    # ax = sns.barplot(x=counts.index, y=counts.values,  palette=colors, color=colors)
    ax.set_xticklabels(ax.get_xticklabels(), rotation=15, ha='center')
    # Bavaria和Jeanneau是Monohulled品牌，Lagoon是

    plt.show()

def visual_metrics_freq():
    X, Y, _ =fullloader()
    regressor = RandomForestRegressor(n_estimators=50)          # 构建新的回归器，用全部数据训练
    regressor = regressor.fit(X, Y)
    data_dic, name_dic =  brandloader()
    res_dic = {'Make Variable':data_dic.keys(), 'Make':[], 'Variant':[], 'is_mono':[], 'MSE':[], 'RMSE':[], 'MAE':[], 'R2':[], 'Count':[]}
    for mv in data_dic.keys(): # mv: Make Variant # 对每个品牌做上面的操作
        # print(name_dic[mv])
        res_dic['Make'].append(name_dic[mv][0])
        res_dic['Variant'].append(name_dic[mv][1])
        res_dic['is_mono'].append(name_dic[mv][2])
        res_dic['Count'].append(name_dic[mv][3])
        # print("***Evaluating %s***" % mv)
        raw = data_dic[mv]
        Y = raw[:, 1]  # Y为第二列
        X = np.concatenate([raw[:, 0].reshape(-1, 1), raw[:, 2:]], axis=1)
        index = [i for i in range(len(X))]
        np.random.shuffle(index)
        X, Y = X[index], Y[index]                               # 随机打散
        Y_pred = regressor.predict(X)
        plt.title(mv)
        mse, rmse, mae, r2 = compute_show_errors(np.exp(Y), np.exp(Y_pred))          # 计算误差度量, 这里的输出记录下来，可以做表
        # predMap(np.exp(Y_pred), np.exp(Y))  # 绘制预测图
        res_dic['MSE'].append(mse)
        res_dic['RMSE'].append(rmse)
        res_dic['MAE'].append(mae)
        res_dic['R2'].append(r2)
    res_df = pd.DataFrame(data=res_dic)             # 建立好df，开始筛选数据
    res_df = res_df.dropna()                        # 缺失值处理
    res_df = res_df[res_df['R2'] > 0]               # 删去不合理的R方
    res_df = res_df[res_df['Count'] < 50]           # 离群点
    columns = ['RMSE', 'MAE', 'R2']
    for i in range(len(columns)):
        col = columns[i]
        plt.subplot(1,3,i+1)
        sns.scatterplot(res_df, x='Count', y=col, hue='is_mono')
        # k = 1
        # x = np.linspace(0.1, 5, 50)
        # y = k / x
        # sns.scatterplot(x=x, y=y, s=50)
        plt.ylabel('')
        plt.title(col)
    # plt.legend()
    plt.show()

def visual_different_models():
    pure_X, pure_Y  = pureloader()
    our_X, our_Y, _ = fullloader()
    '''compare 1:相同模型比较数据提取效果'''
    print("[Compare 1]: Same regressor on different Datasets")
    train_len = int(0.7*len(our_X))
    pure_indexs = [i for i in range(len(pure_X))];   np.random.shuffle(pure_indexs)
    our_indexs = [i for i in range(len(our_X))];     np.random.shuffle(our_indexs)
    pure_X, pure_Y = pure_X[pure_indexs], pure_Y[pure_indexs]
    pure_X, pure_Y = pure_X[:len(our_X)], pure_Y[:len(our_X)] # 数据集长度一致
    # com_model = RandomForestRegressor(n_estimators=50)
    com_model = Lasso()
    com_model = com_model.fit(pure_X[:train_len], pure_Y[:train_len])
    pure_pred = com_model.predict(pure_X[train_len:])
    our_X,  our_Y  = our_X[our_indexs], our_Y[our_indexs]
    our_model = RandomForestRegressor(n_estimators=50)
    our_model = our_model.fit(our_X[:train_len], our_Y[:train_len])
    our_pred = our_model.predict(our_X[train_len:])
    print('Original  data:', end='')
    compute_show_errors(pure_pred, pure_Y[train_len:])
    print('Processed data:', end='')
    compute_show_errors(np.exp(our_pred), np.exp(our_Y[train_len:]))

    '''compare 2:相同数据集比较模型效果'''
    print("[Compare 2]: Same Dataset by different regressors")
    indexs = np.array([i for i in range(len(our_X))])
    np.random.shuffle(indexs)  # 打散数据，防止过拟合
    train_index, test_index = indexs[:train_len], indexs[train_len:]
    X_train, X_test = our_X[train_index], our_X[test_index]
    Y_train, Y_test = our_Y[train_index], our_Y[test_index]
    our_model = RandomForestRegressor(n_estimators=50)
    com_model = Lasso() #0.61
    our_model = our_model.fit(X_train, Y_train)
    com_model = com_model.fit(X_train, np.exp(Y_train))
    our_pred = our_model.predict(X_test)
    com_pred = com_model.predict(X_test)
    print('Ridge Regression Model:', end='')
    compute_show_errors(com_pred, np.exp(Y_test))
    print('Random Forest regressor:', end='')
    compute_show_errors(np.exp(our_pred), np.exp(Y_test))

def visual_fake_bert():
    x = ['380', '39', '400', '421', '450', '52F']
    y = [225000, 312345, 301233,  356231, 468562, 912046]
    y_pred = [351233, 332323, 244423, 300211, 320021, 392334]
    plt.plot([i for i in range(6)], y, label='Real Price')
    plt.plot([i for i in range(6)], y_pred, label='Prediction')
    plt.xlabel("2015 Lagoon xxx")
    plt.xticks(ticks=[i for i in range(6)], labels=x)
    plt.legend()
    plt.show()

def visual_sensitivity():
    x, y, _ = fullloader()
    model = RandomForestRegressor(n_estimators=40)
    model = model.fit(x, y)
    pred  = model.predict(x)
    col = ['Length (ft)', 'Year', 	'LWL (ft)', 'Beam (ft)', 'Draft (ft)', 'Displacement (lbs)', 'Sail Area (sq ft)','GDP per capita (USD)']
    MSE, RMSE, MAE, R2 = compute_show_errors(np.exp(y), np.exp(pred))
    # 扰动1%
    res_dic = {'mse':[], 'rmse':[], 'mae':[], 'r2':[]}
    for i in range(len(col)):
        cur_x = np.array(x.tolist())
        cur_x[:, i] *= 1.01  # 扰动1%
        cur_pred = model.predict(cur_x)
        mse, rmse, mae, r2 = compute_show_errors(np.exp(y), np.exp(cur_pred))
        res_dic['mse'].append(abs(mse-MSE)/MSE)
        res_dic['rmse'].append(abs(rmse-RMSE) / RMSE)
        res_dic['mae'].append(abs(mae-MAE) / MAE)
        res_dic['r2'].append(abs(r2 - R2) / R2)
    res = pd.DataFrame(res_dic, index=col)
    # print(res)
    # sns.barplot(res_dic, x=0)
    res.plot(kind='bar', width=0.8)
    plt.xticks(rotation=20)
    plt.show()



if __name__ == '__main__':
    # heatMap()                                     # 绘制属性之间相关程度的热力图
    # BoxPlot_geo_list()                            # 区域为自变量下，价格的分布变化；显示了数据处理过程
    # visual_price_disp()                           # 绘制价格频数分布图以及去除异常值后的分布
    # visual_rf_importance()                        # 绘制属性重要性的直方图
    # visual_rf_hyperparam()                        # 利用k折交叉验证，度量模型效果，并以此显示超参数选择过程
    # visual_HongkongPredict(model='combined')      # 利用model预测香港数据集，给出预测图和误差
    # compareModels()                               # 分类（单独的mono、cata和总体数据）展示两种随机森林的预测效果区别（区别不大）
    # visual_attribute_distribution()               # 展示数据集中每个属性的分布
    # visual_price_ismono()                         # 展示基于is_mono分开的价格
    # compareModels()                               # 比较统一的分类器和基于is_mono分类的集成分类器
    # visual_MakeVariant_freq()                     # 统计并展示品牌型号的频数，找出最大的用来计算预测效果
    # visual_metrics_freq()                         # 统计三种性能度量和样本数量之间的关系
    # visual_different_models()                     # 展示不同模型之间的效果差异
    # visual_k_fold_validation()                    # 可视化k折交叉验证
    # visual_fake_bert()                            # 模拟Bert预测过程
    visual_sensitivity()                          # 灵敏度分析

