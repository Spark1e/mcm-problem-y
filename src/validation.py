import numpy as np
import pandas as pd
from sklearn.ensemble import RandomForestRegressor, AdaBoostRegressor, BaggingRegressor
from sklearn.linear_model import LinearRegression, Ridge, Lasso
from sklearn.metrics import mean_squared_error
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.neighbors import KNeighborsRegressor
from sklearn.svm import SVR
from sklearn.tree import DecisionTreeRegressor, ExtraTreeRegressor

from load_data import fullloader


def k_fold_validation(X, Y, reg, k=5, shuffle=True):
    indexs = np.arange(0, len(X))
    if shuffle:
        np.random.shuffle(indexs)
    period = int(len(indexs) / k)
    start = 0
    MSEs = []
    for i in range(k):
        train_index = np.concatenate([indexs[:start], indexs[start + period:]], axis=0)
        test_index = indexs[start:start + period]
        X_train, X_test, Y_train, Y_test = X[train_index], X[test_index], Y[train_index], Y[test_index]
        start += period

        reg = reg.fit(X_train, Y_train)
        Y_pred = reg.predict(X_test)

        mse = mean_squared_error(Y_pred, Y_test)
        # print('fold %d, MSE=%.6f' % (i, mse))
        MSEs.append(mse)

    print("MSE on %d-fold-dataset=%.6f" % (k, np.mean(MSEs)))
    return MSEs


if __name__ == '__main__':
    k = 10
    X, Y, _ = fullloader()
    methods = []
    regressors = []
    reg = LinearRegression(); regressors.append(reg);methods.append('Linear')
    reg = KNeighborsRegressor(); regressors.append(reg);methods.append('KNN')
    reg = SVR(); regressors.append(reg);methods.append('SVM')
    reg = Ridge(); regressors.append(reg);methods.append('Ridge')
    reg = Lasso(max_iter=2000); regressors.append(reg);methods.append('Lasso')
    # reg = MLPRegressor(hidden_layer_sizes=(X.shape[-1],), alpha=0.01, max_iter=150); regressors.append(reg);methods.append('MLP')
    reg = DecisionTreeRegressor(); regressors.append(reg) ;methods.append('Decision Tree')
    reg = ExtraTreeRegressor(); regressors.append(reg) ;methods.append('Extra Tree')
    reg = RandomForestRegressor(n_estimators=100); regressors.append(reg);methods.append('Random Forest')
    reg = AdaBoostRegressor(n_estimators=100); regressors.append(reg);methods.append('AdaBoost')
    reg = BaggingRegressor(n_estimators=100); regressors.append(reg);methods.append('Bagging')
    MSEs = []
    for i in range(len(methods)):
        print("Validing regressor ", i)
        mse = k_fold_validation(X, Y, regressors[i], k, shuffle=False)
        MSEs.append(mse)
    # 用seaborn绘图
    MSEs = np.array(MSEs)
    df = pd.DataFrame(MSEs.T, columns=methods)
    sns.lineplot(data=df)
    # 用pyplot绘图
    # x_plot = np.arange(k)
    # for mse in MSEs:
    #     plt.plot(x_plot, mse, label=methods[i])
    # plt.title(f'%d-fold-valiation' % k)
    # plt.legend()
    plt.show()


