import torch
import torch.nn as nn
import inflect
import pandas as pd
from sklearn.decomposition import PCA
from pytorch_pretrained_bert import BertTokenizer, BertModel

device = 'cuda' if torch.cuda.is_available() else 'cpu'
# device = 'cpu'

def str_num2str(input_string):
    # 把字符串中的数字替换为对应的单词，逐数字替换
    p = inflect.engine()
    words = input_string.split()
    chars = []
    for word in words:
        for item in word:
            chars.append(item)
    for i, char in enumerate(chars):
        if char.isdigit():
            chars[i] = p.number_to_words(char)
    output_string = ''.join(chars)
    return output_string

def bert_loader():
    max_len = 15   # 通过bert模型得到的编码一条句子的长度为max_len，过长截断，过短补0，最低15维（只删去一个数据）
    print('loading data from bert...')
    bert_path = 'BERT_MODEL'
    # Monohulled = pd.read_excel('data//Monohulled Sailboats_final.xlsx')
    # Catamarans = pd.read_excel('data//Catamarans_final.xlsx')
    # dataframe = pd.concat([Monohulled, Catamarans], axis=0)
    dataframe = pd.read_csv('data//2023_MCM_data_all.csv', index_col='Make Variant')
    dataframe = dataframe.dropna()
    # print(dataframe['Make Variant'].index[3330])
    dataframe = dataframe.drop("FountainePajotLucia40Owner'sVersion", axis=0)   #超过15维
    Y = dataframe[['LWL (ft)','Beam (ft)', 'Draft (ft)','Displacement (lbs)', 'Sail Area (sq ft)']].values
    Y = torch.tensor(Y, dtype=torch.float32).to(device)
    # print(Y.shape)
    values = dataframe.index.to_numpy()

    tmp = torch.tensor([0 for i in range(768)], dtype=torch.float32).reshape(1,-1,768).to(device) # 用来补零
    res = torch.zeros((1, max_len, 768), dtype=torch.float32).to(device)# 存放结果
    with torch.no_grad():
        bert_model = BertModel.from_pretrained(bert_path).to(device)
        bert_model.eval()
        bert_tokenizer = BertTokenizer.from_pretrained('bert-base-uncased')
        for idx in range(len(values)):
            value = values[idx]
            sentence = str_num2str(value)
            sentence_token = bert_tokenizer.tokenize(sentence)
            sentence_id = bert_tokenizer.convert_tokens_to_ids(sentence_token)
            batch_data = torch.tensor(sentence_id).to(device).reshape(1, -1)
            out, _ = bert_model(batch_data)
            current_out = out[0].to(device)
            if current_out.shape[1] <= max_len:
                for i in range(current_out.shape[1], max_len):
                    current_out = torch.concat([current_out, tmp], dim=1)
            # print(current_out.shape)
            else:
                print(sentence)
                current_out = current_out[:, :max_len, :]
            res = torch.concat([res, current_out], dim=0)
            print("[Loading %.3f%%]" % (100*idx / len(values)))
    return res[1:, :, :], Y


class MyLSTM(nn.Module):
    def __init__(self, input_size, hidden_size, num_layers, output_size, num_epochs, batch_size):
        super(MyLSTM, self).__init__()
        self.hidden_size = hidden_size
        self.num_layers = num_layers
        self.num_epochs = num_epochs
        self.batch_size = batch_size
        self.bn = nn.BatchNorm1d(input_size, affine=False)
        self.lstm = nn.LSTM(input_size, hidden_size, num_layers, batch_first=True)
        self.fc = nn.Sequential(nn.ReLU(), nn.Linear(hidden_size, output_size))
        self.optimizer = torch.optim.Adam(self.parameters(), lr=1e-6)
        self.loss_func = nn.MSELoss().to(device)
        # self.loss_func = nn.CrossEntropyLoss().to(device)
        self.to(device)

    def forward(self, x):
        # Initialize hidden state and cell state
        # batch normalization过程
        batch_size, seq_len, input_size = x.shape
        x = x.transpose(1, 2)  # 将输入的维度顺序从 [batch, seq_len, input_size] 转换为 [batch, input_size, seq_len]
        x = self.bn(x.contiguous().view(-1, input_size))  # 对每个时间步的输入进行标准化
        x = x.view(batch_size, input_size, seq_len).transpose(1, 2)  # 将标准化后的输入转换回原来的维度顺序

        h0 = torch.zeros(self.num_layers, x.size(0), self.hidden_size).to(device=device)
        c0 = torch.zeros(self.num_layers, x.size(0), self.hidden_size).to(device=device)
        # Forward propagate LSTM
        out, _ = self.lstm(x, (h0, c0))
        # Decode the hidden state of the last time step
        out = self.fc(out[:, -1, :])
        return out

    def fit(self, X_train, Y_train):
        self.train()
        if not self.bn.training:
            self.bn.train()
        for epoch in range(self.num_epochs):
            epoch_loss = 0
            for i in range(0, len(X_train), self.batch_size):
                inputs = torch.Tensor(X_train[i:i + self.batch_size])
                labels = torch.Tensor(Y_train[i:i + self.batch_size])
                self.optimizer.zero_grad()
                outputs = self.forward(inputs)
                loss = self.loss_func(outputs, labels)
                loss.backward()
                self.optimizer.step()
                epoch_loss += loss.item()
            print('Epoch [{}/{}], Loss: {:.4f}'.format(epoch + 1, self.num_epochs, epoch_loss))
        torch.save(self, 'MyBertModel//bertLSTM.pth')

    def predict(self, X_test):
        self.eval()
        with torch.no_grad():
            if self.bn.training:
                self.bn.eval()
            test_inputs = torch.Tensor(X_test)
            test_outputs = model(test_inputs)
            return test_outputs


if __name__ == '__main__':
    print('[Device: %s]'%device)
    X, Y = bert_loader()
    # X = torch.randn((3676, 15, 768)).to(device)
    # Y = torch.randn((3676, 5)).to(device)

    # X, Y = X.to('cpu'), Y.to('cpu')
    # print(X.shape, Y.shape)
    # X_reshape = X.view(3676 * 15, 768)
    # pca = PCA(n_components=0.8)
    # X = pca.fit_transform(X_reshape)
    # X = X.reshape(3676, 15, -1)
    # print(X.shape)
    # X, Y = X.to(device), Y.to(device)

    # input_size = X.shape[-1]
    # hidden_size = int(input_size / 3)
    # num_layers = 2
    # output_size = Y.shape[-1]
    # num_epochs = 10
    # batch_size = 32

    # model = MyLSTM(input_size, hidden_size, num_layers, output_size, num_epochs, batch_size)
    # model.fit(X, Y)
    model = torch.load('MyBertModel//bertLSTM.pth').to(device)
    print(model.predict(X[-2:]))