import numpy as np
from sklearn.linear_model import LinearRegression
from sklearn.neighbors import KNeighborsRegressor
from sklearn.svm import SVR
from sklearn.linear_model import Ridge
from sklearn.linear_model import Lasso
from sklearn.tree import DecisionTreeRegressor
from sklearn.tree import ExtraTreeRegressor
from sklearn.ensemble import RandomForestRegressor
from sklearn.ensemble import AdaBoostRegressor
from sklearn.ensemble import BaggingRegressor
from sklearn.metrics import mean_squared_error, r2_score

from visual import predMap
from load_data import dataloader, pureloader, fullloader, store_prediction
from bert import bert_loader
import matplotlib.pyplot as plt

if __name__ == '__main__':
    # X, Y = bert_loader()
    # X, Y = dataloader()
    # X, Y = pureloader()
    X, Y, names_all = fullloader('Catamarans')

    train_len = int(0.9*len(X))
    indexs = np.array([i for i in range(len(X))])
    np.random.shuffle(indexs)
    train_index, test_index = indexs[:train_len], indexs[train_len:]
    X_train, X_test = X[train_index], X[test_index]
    Y_train, Y_test = Y[train_index], Y[test_index]
    # X_train, X_test = X[:train_len], X
    # Y_train, Y_test = Y[:train_len], Y
    names = names_all.values[test_index]


    print("Training regressors...")
    regressors = []
    reg = LinearRegression(); regressors.append(reg)            # 0
    reg = KNeighborsRegressor(); regressors.append(reg)         # 1
    reg = SVR(); regressors.append(reg)                         # 2
    reg = Ridge(); regressors.append(reg)                       # 3
    reg = Lasso(max_iter=2000); regressors.append(reg)          # 4
    # reg = MLPRegressor(hidden_layer_sizes=(X_train.shape[-1],), alpha=0.01, max_iter=150); regressors.append(reg)   # 5
    reg = DecisionTreeRegressor(); regressors.append(reg)       # 5
    reg = ExtraTreeRegressor(); regressors.append(reg)          # 6
    reg = RandomForestRegressor(n_estimators=100); regressors.append(reg)       # 7 #可解释
    reg = AdaBoostRegressor(n_estimators=100); regressors.append(reg)           # 8
    reg = BaggingRegressor(n_estimators=100); regressors.append(reg)            # 9

    idxs = []
    mses = []
    best_idx = 0
    best_mse = np.Inf
    best_pred = []
    for i in range(len(regressors)):
        reg = regressors[i]
        reg = reg.fit(X_train, Y_train)
        Y_pred = reg.predict(X_test)
        # mse = mean_squared_error(Y_test, Y_pred, squared=False)                 # 取log之后的MSE 0.2-0.3
        mse = mean_squared_error(np.exp(Y_test), np.exp(Y_pred), squared=False) # 真实价格的MSE， 60000-90000
        # r2 = r2_score(Y_test, Y_pred)
        print('reg ', i, 'MSE=', mse)
        # print('reg', i, 'r2=',r2)

        if mse < best_mse:
            best_pred = Y_pred
            best_mse = mse
            best_idx = i
        idxs.append(i)
        mses.append(mse)

    plt.plot(idxs, mses)
    plt.show()

    predMap(y_pred=np.exp(best_pred), y_true=np.exp(Y_test))
    print('Linear regressor:', regressors[0].coef_)
    print('Random Forest:', regressors[7].feature_importances_)
    store_prediction(best_pred, Y_test, names)
