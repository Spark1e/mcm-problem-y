'''
这个文件是主函数，获得各小问的结果
'''
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.ensemble import RandomForestRegressor
from Problem_Y.load_data import fullloader, brandloader
from Problem_Y.visual import predMap
from Problem_Y.compute import compute_show_errors

if __name__ == '__main__':

    Problem1, Problem2, Problem3 = True, True, True
    # Problem1 = False
    Problem2 = False
    Problem3 = False
    ''' 第一小问：建模'''
    X, Y, _ = fullloader()
    print("X", X.shape, "Y",Y.shape)
    if Problem1:
        # scalar = StandardScaler()                                 # 用于将输入映射为均值0，方差1
        regressor = RandomForestRegressor(n_estimators=50)          # 构建回归器
        train_len = int(.9*len(X))                                  # 设置训练集大小
        indexs = np.array([i for i in range(len(X))])
        np.random.shuffle(indexs)                                   # 打散数据，防止过拟合
        train_index, test_index = indexs[:train_len], indexs[train_len:]
        X_train, X_test = X[train_index], X[test_index]
        Y_train, Y_test = Y[train_index], Y[test_index]
        regressor.fit(X_train, Y_train)
        Y_pred = regressor.predict(X_test)
        print("Vailding RF model")
        compute_show_errors(np.exp(Y_test), np.exp(Y_pred))         # 计算误差度量
        predMap(np.exp(Y_pred), np.exp(Y_test))                     # 绘制预测图

    regressor = RandomForestRegressor(n_estimators=50)          # 构建新的回归器，用全部数据训练
    regressor = regressor.fit(X, Y)
    Y_pred = regressor.predict(X)
    print("Evaluating model on train set")
    compute_show_errors(np.exp(Y), np.exp(Y_pred))
    predMap(np.exp(Y_pred), np.exp(Y), reallabel='Listing Price', predlabel='Prediction')

    '''第二小问：分别对品牌型号进行精确性分析'''
    if Problem2:
        data_dic, name_dic =  brandloader()
        res_dic = {'Make Variable':data_dic.keys(), 'Make':[], 'Variant':[], 'is_mono':[], 'MSE':[], 'RMSE':[], 'MAE':[], 'R2':[]}
        for mv in data_dic.keys(): # mv: Make Variant # 对每个品牌做上面的操作
            # print(name_dic[mv])
            res_dic['Make'].append(name_dic[mv][0])
            res_dic['Variant'].append(name_dic[mv][1])
            res_dic['is_mono'].append(name_dic[mv][2])
            print("***Evaluating %s***" % mv)
            raw = data_dic[mv]
            Y = raw[:, 1]  # Y为第二列
            X = np.concatenate([raw[:, 0].reshape(-1, 1), raw[:, 2:]], axis=1)
            index = [i for i in range(len(X))]
            np.random.shuffle(index)
            X, Y = X[index], Y[index]                               # 随机打散
            Y_pred = regressor.predict(X)
            plt.title(mv)
            mse, rmse, mae, r2 = compute_show_errors(np.exp(Y), np.exp(Y_pred))          # 计算误差度量, 这里的输出记录下来，可以做表
            # predMap(np.exp(Y_pred), np.exp(Y))  # 绘制预测图
            res_dic['MSE'].append(mse)
            res_dic['RMSE'].append(rmse)
            res_dic['MAE'].append(mae)
            res_dic['R2'].append(r2)
        res_df = pd.DataFrame(data=res_dic)
        res_df.to_excel('data//Problem2_all.xlsx', index=False)

    '''第三小问'''




