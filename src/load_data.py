import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

# 获取数据
# draw = True # 是否数据挖掘
draw = False

pd.set_option('max_colwidth',200)
pd.set_option('display.max_columns', None)

def delete_duplicate(dataframe):
    arr = dataframe.values
    cols = dataframe.columns
    for i in range(len(arr)):
        for j in range(i + 1, len(arr)):
            if i >= len(arr) or j >= len(arr):
                break
            row1 = arr[i, :-1]
            row2 = arr[j, :-1]
            if np.all(row1 == row2):
                print(i, j)
                arr[i, -1] = (arr[i, -1] + arr[j, -1]) / 2
                np.delete(arr, j, axis=0)
    print(arr)
    dataframe = pd.DataFrame(arr, columns=cols)
    return dataframe


def dataloader():
    Monohulled = pd.read_excel('data//2023_MCM_Problem_Y_Boats.xlsx', sheet_name='Monohulled Sailboats ')
    Catamarans = pd.read_excel('data//2023_MCM_Problem_Y_Boats.xlsx', sheet_name='Catamarans')
    dataframe = pd.concat([Monohulled, Catamarans], axis=0)

    sns.displot(dataframe['Listing Price (USD)'])
    plt.show()
    tmpframe = dataframe['Listing Price (USD)'].quantile(0.99)
    dataframe = dataframe[dataframe['Listing Price (USD)'] < tmpframe]
    sns.displot(dataframe['Listing Price (USD)'])
    plt.show()

    # print(dataframe['Year'][0])  # 利用年份到2021年的距离表示
    dataframe.loc[:, 'Year'] = dataframe['Year'].apply(lambda x: 2021 - int(x))
    # print(dataframe)
    # print(dataframe.isnull().sum())
    #
    # print(dataframe.dtypes)

    plt.figure(num=1, figsize=(7, 7))  # 展示数值特征和输出之间的关系----没有关系
    plt.subplot(221)
    plt.scatter(dataframe['Length (ft)'], dataframe['Listing Price (USD)'])
    plt.subplot(222)
    plt.scatter(dataframe['Year'], dataframe['Listing Price (USD)'])
    plt.subplot(223)
    plt.scatter(dataframe['Length (ft)'], np.log(dataframe['Listing Price (USD)']))
    plt.subplot(224)
    plt.scatter(dataframe['Year'], np.log(dataframe['Listing Price (USD)']))
    plt.show()
    # print(dataframe)
    # dataframe = dataframe.drop('Variant', axis=1)
    Makes = set(dataframe['Make'].values)
    make_price_dic = {}
    for make in Makes:
        df = dataframe[dataframe['Make'] == make]
        avg_price = np.mean(df['Listing Price (USD)'])
        make_price_dic[make] = avg_price
    min_val = np.min(list(make_price_dic.values()))
    max_val = np.max(list(make_price_dic.values()))
    x_label = [(make_price_dic[item] - min_val) / (max_val - min_val) for item in dataframe['Make'].values]
    plt.scatter(x_label, dataframe['Listing Price (USD)'])
    plt.show()

    dataframe_dummy = pd.get_dummies(dataframe, columns=['Make', 'Variant', 'Geographic Region',
                                                         'Country/Region/State'])  # 独热编码字符串

    X = dataframe_dummy.drop('Listing Price (USD)', axis=1).values
    Y = dataframe_dummy['Listing Price (USD)'].values

    #
    # pca = PCA(n_components=0.75)
    # X = pca.fit_transform(X)

    print('X shape:', X.shape)
    return X, Y

def Normalization(data):
    return (data - min(data)) / (max(data) - min(data))

def fullloader(typef = 'Both'):
    Monohulled = pd.read_excel('data//Monohulled Sailboats_final.xlsx')
    Catamarans = pd.read_excel('data//Catamarans_final.xlsx')
    if typef == 'Both':
        Monohulled['is_mono'] = 1
        Catamarans['is_mono'] = 0
        dataframe = pd.concat([Monohulled, Catamarans], axis=0)
        dataframe.to_csv('data//2023_MCM_data_all.csv', index=False)
    elif typef == 'Monohulled':
        dataframe = Monohulled
    else:
        dataframe = Catamarans
    names = dataframe[['Make', 'Variant', 'is_mono']] if 'is_mono' in dataframe.columns else dataframe[['Make', 'Variant']]
    dataframe = dataframe.reset_index(drop=True)
    dataframe = dataframe.dropna()  # 消除NaN
    dataframe['Year'] = dataframe['Year'].apply(lambda x: 2020-int(x))                              # 年份做差
    for col in ['Listing Price (USD)', 'Sail Area (sq ft)', 'Displacement (lbs)', 'Draft (ft)']:
        tmpframe = dataframe[col].quantile(0.99)                                                    # 消除长尾
        dataframe = dataframe[dataframe[col] < tmpframe]
    Y = dataframe['Listing Price (USD)'].values
    dummyframe = pd.get_dummies(dataframe[['Geographic Region']])                                   # one hot编码
    droplist = ['Average ratio of total logistics costs to GDP', 'Average Cargo Throughput (tons)',
                'GDP (USD billion)','Country/Region/State', 'Make', 'Variant', 'Make Variant',
                'Geographic Region']
    dataframe = dataframe.drop(droplist, axis=1)
    dataframe = pd.concat([dataframe, dummyframe], axis=1) # 拼接剩余属性和分类属性
    X = dataframe.drop(['Listing Price (USD)'], axis=1).values
    dataframe.to_csv('data//final_dataset.csv', index=False)
    return X, np.log(Y), names
    # return X, Y, names

def load_by_brand():
    Catamarans = pd.read_excel('data//2023_MCM_Problem_Y_Boats.xlsx', sheet_name='Catamarans')
    Catamarans = Catamarans.drop('Variant', axis=1)
    # Catamarans = Catamarans.drop('')
    brands = list(set(Catamarans['Make'].values.tolist()))
    # print(brands)
    for brand in brands:
        brand_df = Catamarans[Catamarans['Make'] == brand]
        print(brand_df)
        plt.scatter(brand_df['Year'], brand_df['Listing Price (USD)'])
        plt.show()


def pureloader():
    # 纯净版，什么都不操作，只有长度、大地区、年份
    Monohulled = pd.read_excel('data//2023_MCM_Problem_Y_Boats.xlsx', sheet_name='Monohulled Sailboats ')
    Catamarans = pd.read_excel('data//2023_MCM_Problem_Y_Boats.xlsx', sheet_name='Catamarans')

    boats = pd.concat([Monohulled, Catamarans], axis=0)
    inputs = boats[['Length \n(ft)', 'Year', 'Geographic Region']]
    labels = boats['Listing Price (USD)'].values
    inputs = pd.get_dummies(inputs, columns=['Geographic Region']).values
    # print(inputs)
    return inputs, labels


def store_prediction(y_pred, y_true, names=None, path='data//Predictions.csv'):
    # 输入预测结果，输出保存在.csv文件的对应价格
    y_pred = np.exp(y_pred)
    y_true = np.exp(y_true)
    df = pd.DataFrame()
    for i in range(len(y_true)):
        if names != None:
            df.at[i, 'Make'] = names[i][0]
            df.at[i, 'Variant'] = str(names[i][1])
            df.at[i, 'is_mono'] = names[i][2]
        df.at[i, 'Real Price'] = y_true[i]
        df.at[i, 'Prediction'] = y_pred[i]
    df.to_csv(path, index=False)
    print("Result saved at", path)

def brandloader():
    # 按照型号输出几个出现频率比较高的型号的帆船数据，以字典形式存储，key：Make Variant, value: df.values
    df = pd.read_csv('data//2023_MCM_data_all.csv')
    df = df.dropna()
    # df = df.drop(['Make','Variant','Country/Region/State',	'Average Cargo Throughput (tons)',
    #               'GDP (USD billion)', 'Average ratio of total logistics costs to GDP'], axis=1)
    dummyframe = pd.get_dummies(df[['Geographic Region']])
    df = df.drop(['Geographic Region'], axis=1)
    df = pd.concat([df, dummyframe], axis=1)
    # df = df.drop(df['Make Variant'] == 'Jeanneau53' and df['Listing Price (USD)'] >370000 )
    # target = ['Lagoon450', 'Lagoon42', 'Lagoon450F', 'Lagoon440', 'Lagoon39', 'Lagoon500','Lagoon40', 'Bali4.1',
    #           'BavariaCruiser46', 'Jeanneau53', 'Bavaria50Cruiser', 'JeanneauSunOdyssey54DS',
    #           'BeneteauOceanis45', 'BeneteauOceanis40', 'BeneteauOceanis50', 'JeanneauSunOdyssey54DS'] # 频率最高的几个品牌型号
    target = df['Make Variant'].values
    df = df[df['Make Variant'].isin(target)]
    data_dict = {} # 以Make Variant为索引，X为值
    name_dict = {}                    # 对应的Make和Variant为值
    count_series = df['Make Variant'].value_counts()
    for col in target:
        # 处理数据集成为能直接运行的输出
        cur_df = df.drop(['Make','Variant','Country/Region/State',	'Average Cargo Throughput (tons)',
                      'GDP (USD billion)', 'Average ratio of total logistics costs to GDP'], axis=1)
        cur_df = cur_df[cur_df['Make Variant'] == col]
        cur_df = cur_df.drop(['Make Variant'], axis=1)
        cur_df['Year'] = cur_df['Year'].apply(lambda x: 2020-int(x))
        cur_df['Listing Price (USD)'] = cur_df['Listing Price (USD)'].apply(lambda x: np.log(x))
        data_dict[col] = cur_df.values
        # 处理Make和Variant
        name_df = df[df['Make Variant'] == col]
        name_df = name_df[['Make', 'Variant', 'is_mono']]
        name_dict[col] = np.append(name_df.values[0], count_series[col])
    return data_dict, name_dict

def merge_GDP_to_data():
    Monohulled = pd.read_excel('data//Monohulled Sailboats_final.xlsx')
    Catamarans = pd.read_excel('data//Catamarans_final.xlsx')
    Monohulled['is_mono'] = 1
    Catamarans['is_mono'] = 0
    df1 = pd.concat([Monohulled, Catamarans], axis=0)
    df2 = pd.read_excel('data//GDPs.xlsx')
    df3 = pd.merge(df1, df2, how='inner', on=['Year', 'Geographic Region'])
    df3.to_csv('data//extended_data_original.csv', index=False)
    # print(df1.shape, df3.shape)
    return df3

def extendloader():
    df = pd.read_csv('data//extended_data_original.csv')


if __name__ == '__main__':
    # fullloader()        # 根据新数据提出的结果，12维
    # pureloader()        # TODO 根据原始数据直接编码
    brandloader()       # 以品牌型号分类提取数据集
    # merge_GDP_to_data() # 把各地区的GDP数据附加到原数据上