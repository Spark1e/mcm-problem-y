import numpy as np
import torch
import torch.nn as nn
import matplotlib.pyplot as plt
from load_data import fullloader

device = 'cuda' if torch.cuda.is_available() else 'cpu'


lr = 0.001
batch_size = 32
class MyMLP(nn.Module):
    def __init__(self, in_dim, hid_dim, out_dim):
        super(MyMLP, self).__init__()
        self.model = nn.Sequential(
            nn.ReLU(),
            nn.Linear(in_dim, hid_dim),
            nn.ReLU(),
            nn.Linear(hid_dim, out_dim)
        )
        self.optimizer = torch.optim.Adam(self.parameters(), lr=lr)
        self.loss_func = nn.MSELoss().to(device)
        self.to(device)

    def forward(self, x):
        return self.model(x)

    def train(self, X, Y, max_epochs=100):
        for epoch in range(max_epochs):
            start = 0
            epoch_loss = 0
            while start < len(X):
                batch_input = X[start:start+batch_size]
                batch_target= Y[start:start+batch_size]
                start += batch_size

                batch_pred = self.forward(batch_input)
                self.optimizer.zero_grad()
                loss = self.loss_func(batch_pred, batch_target)
                loss.backward()
                self.optimizer.step()

                epoch_loss += loss.item()
            print("epoch %d loss:%.3f"%(epoch, epoch_loss))

    def eval(self, X, Y):
        pred = self.forward(X)
        print('MSE=%.3f'%self.loss_func(pred, Y))
        return pred

if __name__ == '__main__':
    X, Y = fullloader()
    X = torch.tensor(X, dtype=torch.float32).to(device)
    Y = torch.tensor(Y, dtype=torch.float32).to(device)
    train_len = int(0.8 * len(X))
    X_train, X_test = X[:train_len], X[train_len:]
    Y_train, Y_test = Y[:train_len], Y[train_len:]
    mlp = MyMLP(X.shape[-1], 2*X.shape[-1], 1)
    mlp.train(X_train, Y_train, max_epochs=1000)
    pred = mlp.eval(X_test, Y_test)
    x_plot = [i for i in range(len(pred))]
    plt.plot(x_plot, Y_test, label='real')
    plt.plot(x_plot, pred, label='pred')
    plt.legend()
    plt.show()




