import numpy as np
from sklearn.metrics import r2_score, mean_squared_error, mean_absolute_error

def compute_show_errors(y_true, y_pred):
    mse = mean_squared_error(y_true, y_pred)
    rmse = np.sqrt(mean_squared_error(y_true, y_pred))
    mae = mean_absolute_error(y_true, y_pred)
    r2  = r2_score(y_true, y_pred)
    print("MSE:  %.4f" % mse)
    print("RMSE: %.4f" % rmse)
    print("MAE:  %.4f" % mae)
    print('R2:   %.4f' % r2)
    return mse, rmse, mae, r2